const express = require('express');
const bodyParser = require('body-parser');

const locationsRouter = express.Router();

locationsRouter.use(bodyParser.json());

locationsRouter.route('/')
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();
    })
    .get((req, res, next) => {
        res.end('Will send all the locations info to you!');
    })
    .post((req, res, next) => {
        res.end('Will add the locations:' + req.body.name + ' with details: ' + req.body.description);
    })
    .put((req, res, next) => {
        res.statusCode = 403;
        res.end('PUT operation not supported on /locations');
    })
    .delete((req, res, next) => {
        res.end('Deleting all locations');
    });


module.exports = locationsRouter;