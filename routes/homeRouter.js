const express = require('express');
const bodyParser = require('body-parser');

const homeRouter = express.Router();

homeRouter.use(bodyParser.json());

homeRouter.route('/')
    .all((req, res, next) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'text/plain');
        next();
    })
    .get((req, res, next) => {
        res.end('Will send all the informations to you!');
    })
    .post((req, res, next) => {
        res.end('Will add the info: ' + req.body.name + ' with details: ' + req.body.description);
    })
    .put((req, res, next) => {
        res.statusCode = 403;
        res.end('PUT operation not supported on /home');
    })
    .delete((req, res, next) => {
        res.end('Deleting all info');
    });


module.exports = homeRouter;