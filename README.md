#  Project Title: :earth_asia: Intelligent Tourist Guide :earth_americas:




## Created by Iftekhar Chowdhury

An app to serve the brave travellers.

####  Mobile View

###### View 1
![Mobile View 1](mv1.png)

###### View 2

![Mobile View 2](mv2.png)

####  Large Screen View

###### View 1
![Mobile View 1](lv1.png)

###### View 2

![Mobile View 1](lv2.png)
